#include "soem_ros2/soem.h"

#include <chrono>
#include <functional>
#include <memory>
#include <thread>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float64.hpp"

#define EC_TIMEOUTMON 500

using namespace std::chrono_literals;

class EthercatNode : public rclcpp::Node
{
public:
	EthercatNode() : Node("EthercatNode")
	{
		std::string interface = "enp3s0";

		ana1_pub = this->create_publisher<std_msgs::msg::Float64>("analog1", 1);
  		ana2_pub = this->create_publisher<std_msgs::msg::Float64>("analog2", 1);

		if (setup_ethercat(interface.c_str()))
		{
			RCLCPP_INFO(this->get_logger(), "Initialization succeeded");
		}
		else
		{
			RCLCPP_ERROR(this->get_logger(), "Initialization failed");
		}

		timer_statecheck = this->create_wall_timer(1000ms, std::bind(&EthercatNode::statecheck_callback, this)); 
		timer_datacycle = this->create_wall_timer(1ms, std::bind(&EthercatNode::datacycle_callback, this));
	}

	~EthercatNode()
	{
		pdo_transfer_active = false;
		RCLCPP_INFO(this->get_logger(), "stop ethercat");
		/* request INIT state for all slaves */
		RCLCPP_INFO(this->get_logger(), "Request init state for all slaves");
		ec_slave[0].state = EC_STATE_INIT;
		ec_writestate(0);
		/* stop SOEM, close socket */
		ec_close();
	}

private:

  void statecheck_callback();
  void datacycle_callback();
  bool setup_ethercat(const char*);

  rclcpp::TimerBase::SharedPtr timer_statecheck;
  rclcpp::TimerBase::SharedPtr timer_datacycle;

  rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr ana1_pub;
  rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr ana2_pub;

  volatile int expectedWKC;
  volatile int wkc;
  bool pdo_transfer_active = false;
};


void EthercatNode::statecheck_callback()
{    
	int slave;
	uint8 currentgroup = 0;

	if (pdo_transfer_active && ((wkc < expectedWKC) || ec_group[currentgroup].docheckstate))
	{
		/* one ore more slaves are not responding */
		ec_group[currentgroup].docheckstate = FALSE;
		ec_readstate();
		for (slave = 1; slave <= ec_slavecount; slave++)
		{
			if ((ec_slave[slave].group == currentgroup) && (ec_slave[slave].state != EC_STATE_OPERATIONAL))
			{
				ec_group[currentgroup].docheckstate = TRUE;
				if (ec_slave[slave].state == (EC_STATE_SAFE_OP + EC_STATE_ERROR))
				{
					RCLCPP_ERROR(this->get_logger(), "ERROR : slave %d is in SAFE_OP + ERROR, attempting ack.", slave);
					ec_slave[slave].state = (EC_STATE_SAFE_OP + EC_STATE_ACK);
					ec_writestate(slave);
				}
				else if (ec_slave[slave].state == EC_STATE_SAFE_OP)
				{
					RCLCPP_WARN(this->get_logger(), "slave %d is in SAFE_OP, change to OPERATIONAL.", slave);
					ec_slave[slave].state = EC_STATE_OPERATIONAL;
					ec_writestate(slave);
				}
				else if (ec_slave[slave].state > 0)
				{
					if (ec_reconfig_slave(slave, EC_TIMEOUTMON))
					{
						ec_slave[slave].islost = FALSE;
						RCLCPP_INFO(this->get_logger(), "MESSAGE : slave %d reconfigured", slave);
					}
				}
				else if (!ec_slave[slave].islost)
				{
					/* re-check state */
					ec_statecheck(slave, EC_STATE_OPERATIONAL, EC_TIMEOUTRET);
					if (!ec_slave[slave].state)
					{
						ec_slave[slave].islost = TRUE;
						RCLCPP_ERROR(this->get_logger(), "slave %d lost", slave);
					}
				}
			}
			if (ec_slave[slave].islost)
			{
				if (!ec_slave[slave].state)
				{
					if (ec_recover_slave(slave, EC_TIMEOUTMON))
					{
						ec_slave[slave].islost = FALSE;
						RCLCPP_INFO(this->get_logger(), "MESSAGE : slave %d recovered", slave);
					}
				}
				else
				{
					ec_slave[slave].islost = FALSE;
					RCLCPP_INFO(this->get_logger(), "MESSAGE : slave %d found", slave);
				}
			}
		}
		if (!ec_group[currentgroup].docheckstate)
		{
			RCLCPP_INFO(this->get_logger(), "OK : all slaves resumed OPERATIONAL.");
		}
	}

	// Extract current thread
	auto curr_thread = std::to_string(std::hash<std::thread::id>()(std::this_thread::get_id()));
	auto info_message = "\n<<statecheck THREAD " + curr_thread + ">> ";
	RCLCPP_INFO(this->get_logger(), info_message);
};

void EthercatNode::datacycle_callback()
{
	if (pdo_transfer_active)
	{
		ec_send_processdata();
		wkc = ec_receive_processdata(EC_TIMEOUTRET);

		std_msgs::msg::Float64 msg;
		float analog;
		uint16_t analog_aid;
		uint8_t * shared_mem_ptr = ec_slave[3].inputs;
		for (int j=0; j<2; j++) 
		{
			analog_aid = *(shared_mem_ptr + 3 + j*4);
			analog_aid = analog_aid << 8; // opschuiven naar eerste 8 bits
			analog_aid = analog_aid + *(shared_mem_ptr + 2 + j*4); // LS 8 bits erbij
			if ((analog_aid >> 15) & 1) // check MSB als 1 dan negatief getal en 'goed' zetten middels two-complements
			{
				analog = (float) ( (analog_aid ^ ((2<<15)-1)) + 1) / -32767.0 * 10.0;
				// gebruik ^ ((2<<15)-1) om inverse te krijgen, want ~analog_aid werkt niet
			}
			else
			{
				analog = (float) (analog_aid) / 32767.0 * 10.0;
			}
			msg.data = analog;
			if (j==0)
			{
				ana1_pub->publish(msg);
			}
			else
			{
				ana2_pub->publish(msg);
			}
		}
	}

	// Extract current thread
	auto curr_thread = std::to_string(std::hash<std::thread::id>()(std::this_thread::get_id()));
	auto info_message = "\n<<datacycle THREAD " + curr_thread + ">> ";
	RCLCPP_INFO(this->get_logger(), info_message);
};


bool EthercatNode::setup_ethercat(const char* ifname)
{
  int i, chk;
  char IOmap[4096];

  /* initialise SOEM, bind socket to ifname */
  if (ec_init(ifname))
  {
    RCLCPP_INFO(this->get_logger(), "ec_init on %s succeeded.", ifname);
    /* find and auto-config slaves */
																																													
    if (ec_config_init(FALSE) > 0)
    {
      RCLCPP_INFO(this->get_logger(), "%d slaves found and configured.", ec_slavecount);

      ec_config_map(&IOmap);

      ec_configdc();

      RCLCPP_INFO(this->get_logger(), "Slaves mapped, state to SAFE_OP.");
      /* wait for all slaves to reach SAFE_OP state */
      ec_statecheck(0, EC_STATE_SAFE_OP, EC_TIMEOUTSTATE * 4);

      RCLCPP_INFO(this->get_logger(), "segments : %d : %d %d %d %d", ec_group[0].nsegments, ec_group[0].IOsegment[0], ec_group[0].IOsegment[1],
               ec_group[0].IOsegment[2], ec_group[0].IOsegment[3]);

      RCLCPP_INFO(this->get_logger(), "Request operational state for all slaves");
      expectedWKC = (ec_group[0].outputsWKC * 2) + ec_group[0].inputsWKC;
      RCLCPP_INFO(this->get_logger(), "Calculated workcounter %d", expectedWKC);
      ec_slave[0].state = EC_STATE_OPERATIONAL;
      /* send one valid process data to make outputs in slaves happy*/
      ec_send_processdata();
      ec_receive_processdata(EC_TIMEOUTRET);
      /* request OP state for all slaves */
      ec_writestate(0);
      chk = 40;
      /* wait for all slaves to reach OP state */

      do
      {
        ec_send_processdata();
        ec_receive_processdata(EC_TIMEOUTRET);
        ec_statecheck(0, EC_STATE_OPERATIONAL, 50000);
      } while (chk-- && (ec_slave[0].state != EC_STATE_OPERATIONAL));
      if (ec_slave[0].state == EC_STATE_OPERATIONAL)
      {
        RCLCPP_INFO(this->get_logger(), "Operational state reached for all slaves.");
        pdo_transfer_active = true;
        /* Check if slaves are found in the expected order */
        return true;
      }
      else
      {
        RCLCPP_WARN(this->get_logger(), "Not all slaves reached operational state.");
        ec_readstate();
        for (i = 1; i <= ec_slavecount; i++)
        {
          if (ec_slave[i].state != EC_STATE_OPERATIONAL)
          {
            RCLCPP_WARN(this->get_logger(), "Slave %d State=0x%2.2x StatusCode=0x%4.4x : %s", i, ec_slave[i].state, ec_slave[i].ALstatuscode,
                     ec_ALstatuscode2string(ec_slave[i].ALstatuscode));
          }
        }
      }
    }
    else
    {
      RCLCPP_ERROR(this->get_logger(), "No slaves found!");
    }
  }
  else
  {
    RCLCPP_ERROR(this->get_logger(), "No socket connection on %s. \n", ifname);
  }
  return false;
}

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  auto enode = std::make_shared<EthercatNode>();
  rclcpp::spin(enode);  
  rclcpp::shutdown();
  return 0;
}
